<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Service\Mcore\Odp;

class McoreOdpSeeder extends Seeder
{
    public function run()
    {
        $wz = DB::table('auth.workzone')
            ->orderBy('id', 'DESC')
            ->select('id')
            ->limit(1)
            ->first()
        ;

        try {
            Odp::create(
                1,
                $wz->id,
                'ODP-SEED-001',
                '',
                0,0,8,
                0,0
            );

            Odp::create(
                1,
                $wz->id,
                'ODP-SEED-002',
                '',
                0,0,8,
                0,0
            );
        } catch (\Throwable $e) { throw $e; }
    }
}
