<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateMcorePelangganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE TABLE mcore.pelanggan(
              id BIGSERIAL PRIMARY KEY,
              workzone_id SMALLINT REFERENCES auth.workzone(id),
              kode TEXT UNIQUE,
              label TEXT CHECK (label <> ''),
              type SMALLINT NOT NULL DEFAULT 0,
              pic TEXT,
              alamat TEXT,
              keterangan TEXT,
              coordinate GEOMETRY(POINT, 4326)
            )
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP TABLE mcore.pelanggan');
    }
}
