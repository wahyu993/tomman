<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateMcoreOdpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE TABLE mcore.odp(
              id BIGSERIAL PRIMARY KEY,
              workzone_id SMALLINT REFERENCES auth.workzone(id),
              label TEXT NOT NULL,
              tenoss TEXT,
              capacity SMALLINT,
              type SMALLINT DEFAULT 0,
              status SMALLINT DEFAULT 0,
              coordinate GEOMETRY(POINT, 4326),
              
              CHECK (label <> '' OR tenoss <> '')
            )
        ");

        DB::statement("CREATE INDEX ON mcore.odp(workzone_id)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP TABLE mcore.odp');
    }
}
