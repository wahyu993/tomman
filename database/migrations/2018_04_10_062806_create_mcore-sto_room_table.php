<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateMcoreStoRoomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE TABLE mcore.sto_room(
              id BIGSERIAL PRIMARY KEY,
              sto_id INTEGER REFERENCES mcore.sto(id),
              label TEXT NOT NULL
            )
        ");

        DB::statement("CREATE INDEX ON mcore.sto_room(sto_id)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP TABLE mcore.sto_room');
    }
}
