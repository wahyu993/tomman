#!/usr/bin/env bash

DIR=`dirname "$(readlink -f "$0")"`/../

# randomize db pass
NEWPASS=($(date +%s%N | md5sum))

sudo -u postgres psql -c "CREATE USER tomman WITH PASSWORD '$NEWPASS'"
sudo -u postgres psql -c "CREATE DATABASE tomman WITH OWNER tomman"
sudo -u postgres psql -d tomman -c "CREATE EXTENSION postgis"
sudo -u postgres psql -d tomman -c "CREATE EXTENSION ltree"

pushd $DIR
sudo sed -i .env \
  -e 's/DB_DATABASE=.*/DB_DATABASE=tomman/' \
  -e 's/DB_USERNAME=.*/DB_USERNAME=tomman/' \
  -e "s/DB_PASSWORD=.*/DB_PASSWORD=$NEWPASS/"

php artisan migrate
popd
