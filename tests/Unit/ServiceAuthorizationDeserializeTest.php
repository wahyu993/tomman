<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Service\Auth\Authorization as Auth;

class ServiceAuthorizationDeserializeTest extends TestCase
{
    public function testEmptyStringReturnsEmptyArray()
    {
        $input = '';
        $expectedResult = [];

        $result = Auth::deserializePermission($input);
        $this->assertEquals($expectedResult, $result);
    }

    public function testSingleEntry()
    {
        $input = '*:ALL';
        $expectedResult = ['*' => Auth::ALL];

        $result = Auth::deserializePermission($input);
        $this->assertEquals($expectedResult, $result);
    }

    public function testSingleEntryWithSpaces()
    {
        $input = ' * : ALL ';
        $expectedResult = ['*' => Auth::ALL];

        $result = Auth::deserializePermission($input);
        $this->assertEquals($expectedResult, $result);
    }

    public function testSingleEntryWithSuperflousCommas()
    {
        $input = '* : ALL,,';
        $expectedResult = ['*' => Auth::ALL];

        $result = Auth::deserializePermission($input);
        $this->assertEquals($expectedResult, $result);
    }

    public function testSingleModuleWithMultiplePermission()
    {
        $input = '* : READ_DATA, READ_AUDIT';
        $expectedResult = ['*' => Auth::READ_DATA | Auth::READ_AUDIT];

        $result = Auth::deserializePermission($input);
        $this->assertEquals($expectedResult, $result);
    }

    public function testMultipleModuleWithSinglePermission()
    {
        $input = '
            mcore.*: READ
            workzone.*: READ
        ';

        $expectedResult = [
            'mcore.*' => Auth::READ,
            'workzone.*' => Auth::READ
        ];

        $result = Auth::deserializePermission($input);
        $this->assertEquals($expectedResult, $result);
    }

    public function testMultipleLinesWithOneEmptyLine()
    {
        $input = '
            meong.*: READ
            mcore.*: READ

            workzone.*: READ
        ';

        $expectedResult = [
            'meong.*' => Auth::READ,
            'mcore.*' => Auth::READ,
            'workzone.*' => Auth::READ
        ];

        $result = Auth::deserializePermission($input);
        $this->assertEquals($expectedResult, $result);
    }

    public function testMultipleLinesWithMultipleEmptyLines()
    {
        $input = '

            one.*: READ

            two.*: READ

            three.*: READ
        ';

        $expectedResult = [
            'one.*' => Auth::READ,
            'two.*' => Auth::READ,
            'three.*' => Auth::READ
        ];

        $result = Auth::deserializePermission($input);
        $this->assertEquals($expectedResult, $result);
    }

    public function testMultipleModuleWithMultiplePermission()
    {
        $input = '
            one.*: READ, READ_AUDIT
            two.*: READ,,
            three.*: READ_DATA, WRITE_DATA, READ_AUDIT,
        ';

        $expectedResult = [
            'one.*' => Auth::READ | Auth::READ_AUDIT,
            'two.*' => Auth::READ,
            'three.*' => Auth::READ_DATA | Auth::WRITE_DATA | Auth::READ_AUDIT
        ];

        $result = Auth::deserializePermission($input);
        $this->assertEquals($expectedResult, $result);
    }
}
