/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const mix = require('laravel-mix');

mix
  .disableNotifications()
  .options({
    purifyCss: true
  })

  .sass('resources/assets/sass/vendor.scss', 'public/css')
  .sass('resources/assets/sass/app.scss', 'public/css')

  .scripts(require('./resources/assets/js/_vendor.mix'), 'public/js/vendor.js')
;


