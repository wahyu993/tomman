<?php $hasError = $errors->has($field) ?>
<fieldset class="form-group form-message-light {{ $hasError?'has-error':'' }}">
  @isset($label)
    <label for="input-{{ $field }}">{!! $label !!}</label>
  @endisset

  @if ($canEdit)
    <textarea id="input-{{ $field }}" name="{{ $field }}" class="form-control"
    @isset($attributes)
      @foreach($attributes as $key => $val)
        @if ($val === true)
          {{ $key }}
        @else
          {{ $key }}="{{ $val }}"
        @endif
      @endforeach
    @endisset
    >{{ old($field, $object->$field ?? '') }}</textarea>

    @isset($help)
      <small class="text-muted">{{ $help }}</small>
    @endisset
  @else
    <p class="form-control-static">{{ $object->$field }}</p>
  @endif

  @if ($hasError)
    @foreach($errors->get($field) as $errorText)
      <small class="form-message light">{{ $errorText }}</small>
    @endforeach
  @endif
</fieldset>
