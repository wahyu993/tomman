<li>
  <div
    class="tree-selectable"
    data-id="{{ $tree->id }}"
    data-nama="{{ $tree->label }}"
  >{{ $tree->label }}</div>

  @if (count($tree->children))
    <ul class="tree">
      @each('partial.tree', $tree->children, 'tree')
    </ul>
  @endif
</li>
