@extends('app')

@section('title', $roomData->label)

@section('body')
  <ol class="breadcrumb page-breadcrumb">
    <li>
      <a href="/mcore/sto/workzone/{{ $stoData->workzone_id }}">
        <span class="label label-primary">WZ</span>
        <span>{{ $stoData->workzone_label }}</span>
      </a>
    </li>
    <li>
      <a href="/mcore/sto/{{ $stoData->id }}">
        <span class="label label-primary">STO</span>
        <span>{{ $stoData->label }}</span>
      </a>
    </li>
    <li class="active">
      <span class="label label-primary">room</span>
      <span>{{ $roomData->label }}</span>
    </li>
  </ol>

  <div class="page-header">
    <h1>
      <i class="fas fa-edit"></i>
      <span>{{ $roomData->label }}</span>
    </h1>
  </div>

  <form id="form" method="post" class="panel">
    {{ csrf_field() }}
    <div class="panel-body">
      @include('partial.form.text', [
        'label' => 'Nama/Label Ruangan',
        'object' => $roomData,
        'field' => 'label',
        'canEdit' => $canEdit,
        'attributes' => [
          'required' => true,
          'data-msg-required' => 'Silahkan isi data ini'
        ]
      ])
    </div>

    @if ($canEdit)
      <div class="panel-footer">
        <div class="row">
          <div class="col-sm-2 col-sm-push-10 m-xs-b-20 ">
            <button class="btn btn-primary width-xs-full pull-sm-right">
              <i class="fas fa-check"></i>
              <span>Simpan</span>
            </button>
          </div>

          <div class="col-xs-6 col-md-3 col-lg-2 col-sm-pull-2">
            <a href="/mcore/sto/{{ $stoData->id }}/room/{{ $roomData->id }}/olt/new" class="btn btn-info width-full">
              <i class="fas fa-plus"></i>
              <span>Input OLT</span>
            </a>
          </div>

          <div class="col-xs-6 col-md-3 col-lg-2 col-sm-pull-2">
            <a href="/mcore/sto/{{ $stoData->id }}/room/{{ $roomData->id }}/odf/new" class="btn btn-info width-full">
              <i class="fas fa-plus"></i>
              <span>Input ODF</span>
            </a>
          </div>
        </div>
      </div>
    @endif
  </form>

  @if (count($odfGroup))
    <h3>ODF</h3>
    @foreach($odfGroup as $group => $odfList)
      <div class="panel">
        <div class="panel-heading">
          <span class="panel-title">{{ $group }}</span>
        </div>

        <div class="panel-body">
          <ul class="list-blocks">
            @foreach($odfList as $odf)
              <li>
                <a href="/mcore/sto/{{ $stoData->id }}/room/{{ $roomData->id }}/odf/{{ $odf->id }}">{{ $odf->label }}</a>
              </li>
            @endforeach
          </ul>
        </div>
      </div>
    @endforeach
  @endif

@endsection

@section('script')
  @include('partial.form.validate', ['id' => 'form'])
@endsection
