<script>
  window.McoreMap.init = (() => {
    const $mapView = $('#mcore_map');
    const mapView = $mapView[0];

    const c = window.McoreMap.const;

    const defaultZoom = c.defaultZoom;
    const defaultCenterPoint = {
      lat: c.center.lat,
      lng: c.center.lng
    };

    let zoom;
    let center;

    let mapState = localStorage.getItem('mapstate');
    if (mapState) {
      try {
        mapState = JSON.parse(mapState);
        zoom = mapState.zoom;
        center = {
          lat: mapState.lat,
          lng: mapState.lng
        };
      } finally {}
    } else {
      zoom = defaultZoom;
      center = defaultCenterPoint;
    }

    const onMapStateChanged = () => {
      const mapCenter = map.getCenter();

      localStorage.setItem('mapstate', JSON.stringify({
        zoom: map.getZoom(),
        lat: mapCenter.lat(),
        lng: mapCenter.lng(),
      }));
    };

    const init = () => {
      const footerHeight = $('.px-footer-bottom').outerHeight();
      $mapView.css('height', window.innerHeight - footerHeight);

      window.map = new google.maps.Map(mapView, {
        center: center,
        zoom: zoom,
        minZoom: defaultZoom,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        clickableIcons: false
      });

      google.maps.event.addListener(window.map, 'idle', onMapStateChanged);

      const features = window.McoreMap.features;
      Object.keys(features).forEach(key => {
        features[key].init();
      });
    };

    return init;
  })();
</script>
