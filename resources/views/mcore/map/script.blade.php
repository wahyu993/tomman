<script>
  window.McoreMap = (() => {
    const $loadingIndicator = $('#loading-indicator');
    let loadingCount = 0;

    const updateLoadingIndicatorVisibility = () => {
      if (loadingCount) {
        $loadingIndicator.removeClass('hidden');
      } else {
        $loadingIndicator.addClass('hidden');
      }
    };

    const incrementLoading = () => {
      loadingCount++;
      updateLoadingIndicatorVisibility();
    };

    const decrementLoading = () => {
      loadingCount--;
      updateLoadingIndicatorVisibility();
    };

    return {
      incrementLoading,
      decrementLoading,

      features: {}
    }
  })();
</script>
