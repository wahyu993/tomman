<script>
  window.McoreMap.features.sto = (() => {
    const mcoreMap = window.McoreMap;

    let clickHandlerCallback;
    const setClickHandler = clickHandler => clickHandlerCallback = clickHandler;
    const intermediateHandler = (marker, event) => {
      if (!clickHandlerCallback) return;

      clickHandlerCallback(marker, event);
    };

    const init = (clickHandler) => {
      setClickHandler(clickHandler);

      mcoreMap.incrementLoading();

      $.ajax({
        url: '/mcore/map/sto',
        success: result => {
          mcoreMap.decrementLoading();

          const markers = [];
          result.forEach(data => {
            data.deviceType = mcoreMap.const.types.sto;
            const marker = new google.maps.Marker({
              //map: window.map,
              data,
              icon: '/img/mcore/sto.png',
              position: {
                lat: Number(data.lat),
                lng: Number(data.lng),
              }
            });
            markers.push(marker);

            marker.addListener('click', event => intermediateHandler(marker, event));
          });

          new MarkerClusterer(
            window.map,
            markers,
            {
              maxZoom: 14,
              averageCenter: true,
              styles: [
                {
                  url: '/img/mcore/sto-radius-1.png',
                  height: 92,
                  width: 64,
                  textColor: 'white',
                  textSize: 16,
                  anchor: [24,0],
                }
              ]
            }
          );
        },
        error: (xhr, status, err) => {
          mcoreMap.decrementLoading();
          // TODO
          console.log('Failed to load STO', status, err);
        }
      });
    };

    return {
      init,
      setClickHandler
    }
  })();
</script>
