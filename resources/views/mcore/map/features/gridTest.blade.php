<script>
  window.McoreMap.features.gridTest = (() => {
    const mcoreMap = window.McoreMap;

    let map;
    let rects = [];

    const loadCell = (zoom, x, y) => {
      let exists = rects.find(rect => rect.x === x && rect.y === y);
      if (exists) return;

      const rect = { x, y };
      rects.push(rect);

      $.ajax({
        url: `/mcore/map/cellTest/${zoom}/${x},${y}`,
        success: result => {
          rect.mapObject = new google.maps.Rectangle({
            strokeColor: '#ff0000',
            strokeWeight: 1,
            fillColor: '#000000',
            fillOpacity: .3,
            map: window.map,
            bounds: {
              north: result.n,
              south: result.s,
              east: result.e,
              west: result.w
            }
          });
        },
        error: (xhr, status, err) => {
          console.log('Failed to load cell', status, err);
        }
      });
    };

    const onMapIdle = () => {
      const zoom = map.getZoom();
      const latlng = map.getCenter();
      const cell = mcoreMap.grid.calcGridCell(zoom, latlng.lat(), latlng.lng());
      if (!cell) return;

      const {x,y} = cell;
      [
        { zoom, x, y },
        { zoom, x: x-1, y },
        { zoom, x: x+1, y },
        { zoom, x, y: y-1 },
        { zoom, x, y: y+1 },
        { zoom, x: x+1, y: y+1 },
        { zoom, x: x+1, y: y-1 },
        { zoom, x: x-1, y: y+1 },
        { zoom, x: x-1, y: y-1 }
      ].forEach(cell => loadCell(cell.zoom, cell.x, cell.y));
    };

    const init = () => {
      map = window.map;
      google.maps.event.addListener(map, 'idle', onMapIdle);
    };

    return {
      init
    }
  })();
</script>
