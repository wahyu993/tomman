<script>
  window.McoreMap.features.grid = (() => {
    const mcoreMap = window.McoreMap;

    let map;
    let loadedCells = {};
    let layers = {};

    let clickHandlerCallback;
    const setClickHandler = clickHandler => clickHandlerCallback = clickHandler;
    const intermediateHandler = (marker, event) => {
      if (!clickHandlerCallback) return;

      clickHandlerCallback(marker, event);
    };

    const loadCell = (type, zoom, x, y) => {
      // TODO: mapstate: layer visibility

      if (loadedCells[type]) {
        let exists = loadedCells[type].find(cell => cell.x === x && cell.y === y);
        if (exists) return;
      } else {
        loadedCells[type] = [];
      }

      const cell = {x, y};
      loadedCells[type].push(cell);

      const url = `/mcore/map/grid/${type}/${zoom}/${x},${y}`;
      mcoreMap.incrementLoading();
      $.ajax({
        url,
        success: result => {
          if (!layers[type]) layers[type] = [];

          const icon = mcoreMap.const.marker[type] ? mcoreMap.const.marker[type]() : `/img/mcore/${type}.png`;
          result.forEach(data => {
            data.deviceType = type;
            const marker = new google.maps.Marker({
              map,
              data,
              icon,
              position: {
                lat: Number(data.lat),
                lng: Number(data.lng)
              }
            });

            cell.marker = marker;
            cell.data = data;
            layers[type].push(marker);

            marker.addListener('click', event => intermediateHandler(marker, event));
          });

          mcoreMap.decrementLoading();
        },
        error: (xhr, status, err) => {
          console.log('Failed to load cell', status, err, {type, zoom, x, y});
          mcoreMap.decrementLoading();
        }
      });
    };

    const onMapIdle = () => {
      const zoom = map.getZoom();
      const latlng = map.getCenter();
      const cell = mcoreMap.grid.calcGridCell(zoom, latlng.lat(), latlng.lng());
      if (!cell) {
        Object.keys(layers)
          .forEach(type => layers[type].forEach(marker => marker.setVisible(false)));

        return;
      }

      Object.keys(layers)
        .forEach(type => layers[type].forEach(
          marker => marker.setVisible(cell.load.includes(type) || cell.visible.includes(type))
        ));

      const {x,y} = cell;
      [
        { x, y },
        { x: x-1, y },
        { x: x+1, y },
        { x, y: y-1 },
        { x, y: y+1 },
        { x: x+1, y: y+1 },
        { x: x+1, y: y-1 },
        { x: x-1, y: y+1 },
        { x: x-1, y: y-1 }
      ].forEach(c => cell.load.forEach(type => loadCell(type, zoom, c.x, c.y)));
    };

    const init = (clickHandler) => {
      setClickHandler(clickHandler);

      map = window.map;
      google.maps.event.addListener(map, 'idle', onMapIdle);
    };

    return {
      init,
      setClickHandler
    }
  })();
</script>
