<div>
  <span>Tube</span>
  <span class="label label-outline outline-fiber-{{ $tubeNum }}">{{ $tubeNum }}</span>
  <span>Core</span>
  <span class="label label-outline outline-fiber-{{ $coreNum }}">{{ $coreNum }}</span>
  <span>#{{ $coreId }}</span>

  @isset($link)
    <i class="fas fa-long-arrow-alt-right text-info"></i>
    @if ($link->dst_type === 'odp')
      <span class="label label-primary">ODP</span>
      <span>{{ $link->odp_label }}</span>
    @elseif ($link->dst_type === 'odc')
      <span class="label label-primary">ODC</span>
      <span>{{ $link->odc_label }}</span>
    @endif
  @endisset
</div>
<div class="bg-fiber-{{ $coreNum }}" style="height:4px"></div>
