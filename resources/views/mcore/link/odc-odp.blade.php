@extends('app')

@section('title', 'Sambungan Distribusi')

@section('body')
  @if(Request::is('*/odc/*'))
    <ol class="breadcrumb page-breadcrumb">
      <li>
        <a href="/mcore/odp/workzone/{{ $linkData->odc_workzone_id }}">
          <span class="label label-primary">WZ</span>
          <span>{{ $linkData->odc_workzone_label }}</span>
        </a>
      </li>
      <li>
        <a href="/mcore/odc/{{ $linkData->odc_id }}">
          <span class="label label-primary">ODC</span>
          <span>{{ $linkData->odc_label }}</span>
        </a>
      </li>
      <li class="active">
        <span>PANEL</span>
        <span class="label label-outline label-info">{{ $linkData->odc_panel }}</span>
        <span>PORT</span>
        <span class="label label-outline label-info">{{ $linkData->odc_port }}</span>
      </li>
    </ol>
  @else
    <ol class="breadcrumb page-breadcrumb">
      <li>
        <a href="/mcore/pelanggan/workzone/{{ $linkData->odp_workzone_id }}">
          <span class="label label-primary">WZ</span>
          <span>{{ $linkData->odp_workzone_label }}</span>
        </a>
      </li>
      <li>
        <a href="/mcore/odp/{{ $linkData->odp_id }}">
          <span class="label label-primary">ODP</span>
          <span>{{ $linkData->odp_label }}</span>
        </a>
      </li>
      <li class="active">
        <span>Distribusi</span>
      </li>
    </ol>
  @endisset

  <div class="page-header">
    <h1>
      <i class="fas fa-share-alt"></i>
      <span>Sambungan Distribusi</span>
    </h1>
  </div>

  <form id="form" method="post" class="panel">
    {{ csrf_field() }}

    <div class="panel-body">
      <div class="row">
        @isset($linkData->odc_id)
          <input name="src_id" value="{{ $linkData->odc_id }}" type="hidden">
          <input name="src_val" value="{{ $linkData->src_val }}" type="hidden">

          <div class="col-md-4">
            <div class="form-group">
              <label>ODC</label>
              <div class="form-control-static">
                <input name="src_id" value="{{ $linkData->odc_id }}" type="hidden">
                <a href="/mcore/odc/{{ $linkData->odc_id }}">{{ $linkData->odc_label }}</a>
              </div>
            </div>

            <div class="form-group">
              <label>Panel &amp; Port</label>
              <div class="form-control-static">
                <input name="src_val" value="{{ $linkData->src_val }}" type="hidden">
                <span>Panel</span>
                <span class="label label-outline label-info">{{ $linkData->odc_panel }}</span>
                <span>Port</span>
                <div class="label label-outline label-info">{{ $linkData->odc_port }}</div>
              </div>
            </div>
          </div>
        @else
          <div class="col-md-4">
            <div class="row">
              <div class="col-xs-12">
                @include('partial.form.select.keyval', [
                  'label' => 'ODC',
                  'object' => $linkData,
                  'field' => 'src_id',
                  'options' => [],
                  'canEdit' => true,
                  'attributes' => [
                    'required' => true,
                    'data-msg-required' => 'Silahkan pilih ODC'
                  ]
                ])
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12">
                <div id="panel-form-group">
                  @include('partial.form.select.keyval', [
                    'label' => 'Panel & Port',
                    'object' => $linkData,
                    'field' => 'src_val',
                    'options' => [],
                    'canEdit' => true,
                    'attributes' => [
                      'required' => true,
                      'data-msg-required' => 'Silahkan pilih Port ODC'
                    ]
                  ])
                </div>
              </div>
            </div>
          </div>
        @endisset

        <div id="distribusi-form-group" class="col-md-4">
          @isset($linkData->distribusi_id)
            <input name="med_id" value="{{ $linkData->distribusi_id }}" type="hidden">
            <input name="med_val" value="{{ $linkData->med_val }}" type="hidden">

            <div class="form-group">
              <label>Kabel Distribusi</label>
              <div class="form-control-static">
                <a href="/mcore/distribusi/{{ $linkData->distribusi_id }}">{{ $linkData->distribusi_label }}</a>
              </div>
            </div>

            <div class="form-group">
              <label>Core</label>
              @include('mcore.link.partial.tubecore', \App\Service\Mcore\Helper::getCoreData($linkData->med_val))
            </div>
          @else
            <div>
              <label>Kabel Distribusi</label>
              <div class="row">
                <div class="col-xs-10">
                  @include('partial.form.select.keyval', [
                    'object' => $linkData,
                    'field' => 'med_id',
                    'options' => [],
                    'canEdit' => true,
                    'attributes' => [
                      'required' => true,
                      'data-msg-required' => 'Silahkan pilih Kabel Distribusi'
                    ]
                  ])
                </div>
                <div class="col-xs-2">
                  <a href="/mcore/distribusi/new?ret={{ url()->full() }}" class="btn btn-info pull-right">
                    <i class="fas fa-plus"></i>
                  </a>
                </div>
              </div>
            </div>

            @include('partial.form.select.keyval', [
              'label' => 'Core',
              'object' => $linkData,
              'field' => 'med_val',
              'options' => [],
              'canEdit' => true,
              'attributes' => [
                'required' => true,
                'data-msg-required' => 'Silahkan pilih Core'
              ]
            ])
          @endisset
        </div>

        <div class="col-md-4">
          @isset($linkData->odp_id)
            <input name="dst_id" value="{{ $linkData->odp_id }}" type="hidden">

            <div class="form-group">
              <label>ODP</label>
              <div>
                <a href="/mcore/odp/{{ $linkData->odp_id }}">{{ $linkData->odp_label }}</a>
              </div>
            </div>
          @else
            @include('partial.form.select.keyval', [
              'label' => 'ODP',
              'object' => $linkData,
              'field' => 'dst_id',
              'options' => [],
              'canEdit' => true,
              'attributes' => [
                'required' => true,
                'data-msg-required' => 'Silahkan pilih ODP'
              ]
            ])
          @endisset
        </div>
      </div>
    </div>

    @if ($canEdit)
      @isset($linkData->med_id)
        <div class="panel-footer">
          <button name="action" value="unplug" type="submit" class="btn btn-warning">
            <i class="fas fa-unlink"></i>
            <span>Cabut Sambungan</span>
          </button>
        </div>
      @else
        <div class="panel-footer text-right">
          <button name="action" value="plug" type="submit" class="btn btn-primary">
            <i class="fas fa-check"></i>
            <span>Simpan</span>
          </button>
        </div>
      @endisset
    @endif
  </form>
@endsection

@section('script')
  @empty($linkData->med_val)
    <script>
      $(() => {
        const $odcInput = $('#input-src_id');
        const $panelFormGroup = $('#panel-form-group');
        const $panelInput = $('#input-src_val');

        const $distribusiFormGroup = $('#distribusi-form-group');
        const $distribusiInput = $('#input-med_id');
        const $coreInput = $('#input-med_val');

        const $odpInput = $('#input-dst_id');

        const placeholder = '&nbsp;';
        const escapeMarkup = m => m;
        const pagedDataHandler = params => {
          return {
            q: params.term,
            page: params.page || 1
          }
        };

        if ($odcInput.length) {
          $odcInput.select2({
            ajax: {
              url: '/mcore/odc/workzone/{{ $linkData->odp_workzone_id ?? 0 }}.select2',
              data: pagedDataHandler
            },
            placeholder,
            escapeMarkup
          });

          $odcInput.on('select2:select', event => {
            if ($panelInput.hasClass('select2-hidden-accessible')) {
              $panelInput.select2('destroy').empty();
            }
            $panelFormGroup.addClass('form-loading');

            const id = event.params.data.id;
            $.get('/mcore/odc/'+id+'/rearport.select2', data => {
              $panelInput.select2({
                data,
                placeholder,
                escapeMarkup
              });

              $panelFormGroup.removeClass('form-loading');
            });
          });
        }

        $distribusiInput.select2({
          ajax: {
            url: '/mcore/distribusi/link/workzone/{{ $linkData->odc_workzone_id ?? $linkData->odp_workzone_id }}.select2',
            data: pagedDataHandler
          },
          placeholder,
          escapeMarkup
        });

        $distribusiInput.on('select2:select', event => {
          if ($coreInput.hasClass('select2-hidden-accessible')) {
            $coreInput.select2('destroy').empty();
          }
          $distribusiFormGroup.addClass('form-loading form-loading-inverted');

          const id = event.params.data.id;
          $.get('/mcore/distribusi/'+id+'/core.select2', data => {
            $coreInput.select2({
              data,
              placeholder,
              escapeMarkup
            });

            $distribusiFormGroup.removeClass('form-loading form-loading-inverted');
          });
        });

        if ($odpInput.length) {
          $odpInput.select2({
            ajax: {
              url: '/mcore/odp/workzone/{{ $linkData->odc_workzone_id ?? 0 }}/link.select2',
              data: pagedDataHandler
            },
            placeholder,
            escapeMarkup
          });
        }

      });
    </script>
  @endempty

  @include('partial.form.validate', ['id' => 'form'])
@endsection
