<script>
  if (!window.Alpro) window.Alpro = {};
  window.Alpro.grid = (() => {
    const mcoreMap = window.McoreMap;
    const c = mcoreMap.const;

    let map;
    let lastZoom = 0;
    let lastVisibility = null;
    let loadedCells = [];
    let markers = [];

    let clickHandlerCallback;
    const setClickHandler = clickHandler => clickHandlerCallback = clickHandler;
    const intermediateHandler = (marker, event) => {
      if (!clickHandlerCallback) return;

      clickHandlerCallback(marker, event);
    };

    const updateVisibility = (zoom, isVisible) => {
      if (zoom === lastZoom || isVisible === lastVisibility) return;

      markers.forEach(marker => marker.setVisible(isVisible));

      lastZoom = zoom;
      lastVisibility = isVisible;
    };

    const loadCell = (zoom, x, y) => {
      const exists = loadedCells.find(cell => cell.x === x && cell.y === y);
      if (exists) return;

      const cell = {x,y};
      loadedCells.push(cell);

      const url = `/mcore/map/grid/${c.types.alpro}/${zoom}/${x},${y}`;
      mcoreMap.incrementLoading();
      $.ajax({
        url,
        success: result => {
          cell.markers = [];
          const icon = c.marker.alpro();
          result.forEach(data => {
            data.deviceType = c.types.alpro;
            data.typeAsText = c.enums.alproTypes[data.type];
            const marker = new google.maps.Marker({
              map,
              data,
              icon,
              position: {
                lat: Number(data.lat),
                lng: Number(data.lng)
              }
            });

            markers.push(marker);
            cell.markers.push(marker);

            marker.addListener('click', event => intermediateHandler(marker, event));
          });

          mcoreMap.decrementLoading();
        },
        error: (xhr, status, err) => {
          console.log('Failed to load cell', status, err, {zoom, x, y});
          mcoreMap.decrementLoading();
        }
      });
    };

    const onMapIdle = () => {
      const zoom = map.getZoom();
      const latlng = map.getCenter();
      const cell = mcoreMap.grid.calcGridCell(zoom, latlng.lat(), latlng.lng());
      if (!cell || !cell.load.includes(c.types.alpro)) {
        updateVisibility(zoom, false);
        return;
      }

      updateVisibility(zoom, true);

      const {x,y} = cell;
      [
        { x, y },
        { x: x-1, y },
        { x: x+1, y },
        { x, y: y-1 },
        { x, y: y+1 },
        { x: x+1, y: y+1 },
        { x: x+1, y: y-1 },
        { x: x-1, y: y+1 },
        { x: x-1, y: y-1 }
      ].forEach(c => loadCell(zoom, c.x, c.y));
    };

    const init = (clickHandler) => {
      setClickHandler(clickHandler);

      map = window.map;
      map.addListener('idle', onMapIdle);
    };

    return {
      init,
      setClickHandler
    };
  })();
</script>
