@extends('app')

<?php if (!isset($odcData)) $odcData = null ?>
<?php $title = $odcData->label ?? 'Input ODC' ?>

@section('title', $title)

@section('style')
  <?php $odcPanelWidth = (($odcData->portperpanel ?? 1) * 64) + 100 ?>
  <style>
    .odc-panel {
      padding: 10px;
      margin-bottom: 20px;
      background-color: #374049;
      border: 1px solid #3f4851;
      min-width: {{ $odcPanelWidth }}px;
    }
    .odc-panel .caption {
      display: inline-block;
      width: 100px;
      font-weight: bold;
    }
    .odc-panel .port-column {
      display: inline-block;
      width: 40px;
      margin-right: 32px;
    }

    .clickable:hover {
      background-color: #272e35;
      cursor: pointer;
    }

    .list-splitter {
      list-style-type: none;
      padding: 0;
      margin: 0;
    }
    .list-splitter > li {
      display: inline-block;
      text-align: center;
      height: 30px;
      padding: 5px 10px 0;
      margin-right: 5px;
      cursor: pointer;
      border: 1px solid black;
    }
    .list-splitter > li:hover {
      border-color: #59c4e4;
    }

    .splitter-port {
      margin: 4px 0;
      padding: 4px;
    }

    .port, .rearport {
      cursor: pointer;
    }
    .port.in {
      border-width: 2px;
      border-color: black;
    }
    .port:hover, .rearport:hover {
      border-color: #59c4e4;
    }

    hr {
      margin: 2px 0;
    }

    @media(min-width: 768px) {
      .input-group-btn > button {
        text-align: left;
        width: 95px;
      }
    }
  </style>
@endsection

@section('body')
  @if (isset($odcData->id))
    <ol class="breadcrumb page-breadcrumb">
      <li>
        <a href="/mcore/odc/workzone/{{ $odcData->workzone_id }}">
          <span class="label label-primary">WZ</span>
          <span>{{ $odcData->workzone_label }}</span>
        </a>
      </li>
      <li class="active">
        <span class="label label-primary">ODC</span>
        <span>{{ $odcData->label }}</span>
      </li>
    </ol>
  @endif

  <div class="page-header">
    <h1>
      <i class="fas fa-edit"></i>
      <span>
        @if (isset($odcData))
          {{ $odcData->label }}
        @else
          Input ODC
        @endif
      </span>
    </h1>
  </div>

  <form id="form-odc" class="panel" method="post">
    {{ csrf_field() }}

    <div class="panel-body">
      <div class="row">
        <div class="col-md-8">
          <div class="row">
            <div class="col-md-6">
              @include('partial.form.text', [
                'label' => 'Nama ODC',
                'object' => $odcData,
                'field' => 'label',
                'canEdit' => $canEdit,
                'attributes' => [
                  'required' => true,
                  'data-msg-required' => 'Silahkan isi nama ODC'
                ]
              ])
            </div>

            <div class="col-md-6">
              @include('partial.workzone.formcontrol', [
                'label' => 'Work Zone',
                'object' => $odcData,
                'field' => 'workzone_id',
                'displayField' => 'workzone_label',
                'canEdit' => $canEdit,
                'workzoneTree' => $workzoneTree,
                'attributes' => [
                  'required' => true,
                  'data-msg-required' => 'Silahkan pilih Area Kerja'
                ]
              ])
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              @include('partial.mapmarker.formcontrol', [
                'label' => 'Koordinat (lat, lng)',
                'object' => $odcData,
                'field' => 'coordinate',
                'canEdit' => $canEdit
              ])
            </div>
          </div>
        </div>

        <div class="col-md-4">
          <div class="row">
            <div class="col-md-6 col-xs-6">
              @include('partial.form.text', [
                'label' => 'Jumlah Panel',
                'object' => $odcData,
                'field' => 'panelcount',
                'canEdit' => $canEdit,
                'attributes' => [
                  'type' => 'number',
                  'min' => 1,
                  'max' => 500,
                  'required' => true,
                  'data-msg-required' => 'Silahkan isi Jumlah Panel'
                ]
              ])
            </div>

            <div class="col-md-6 col-xs-6">
              @include('partial.form.select.val', [
                'label' => 'Port per Panel',
                'object' => $odcData,
                'field' => 'portperpanel',
                'options' => $ports,
                'canEdit' => $canEdit
              ])
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              @include('partial.form.select.keyval', [
                'label' => 'Tipe',
                'object' => $odcData,
                'field' => 'type',
                'options' => $types,
                'canEdit' => $canEdit
              ])
            </div>
          </div>
        </div>
      </div>
    </div>

    @if ($canEdit)
      <div class="panel-footer text-right">
        <button class="btn btn-primary">
          <i class="fas fa-check"></i>
          <span>Simpan</span>
        </button>
      </div>
    @endif
  </form>

  @if (isset($odcData->id))
    @include('mcore.odc.splitter')
    @include('mcore.odc.panelport')

    @include('mcore.odc.modal.splitter')
    @include('mcore.odc.modal.frontlink')
    @include('mcore.odc.modal.rearlink')
  @endif

@endsection

@section('script')
  @include('partial.mapmarker.modal')
  @include('partial.mapmarker.script', ['field' => 'coordinate', 'canEdit' => $canEdit])

  @include('partial.workzone.modal')
  @include('partial.workzone.script', [
    'field' => 'workzone_id',
    'displayField' => 'workzone_label',
    'workzoneTree' => $workzoneTree,
    'canEdit' => $canEdit
  ])

  @include('partial.form.validate', ['id' => 'form-odc'])

  @if (isset($odcData->id))
    @include('mcore.odc.script.splitterlist')
    @include('mcore.odc.script.elements')
    @include('mcore.odc.script.splitter')
    @include('mcore.odc.script.frontlink')
    @include('mcore.odc.script.rearlink')

    @include('partial.form.validate', ['id' => 'form-splitter'])
    @include('partial.form.validate', ['id' => 'form-splitter-edit'])
  @endif
@endsection
