@if (count($splitterList))
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">Splitter</h4>
    </div>
    <div class="panel-body">
      <ul id="splitter-list" class="list-splitter">
        <?php $colorIndex = 1; $maxColors = \App\Service\Mcore\Helper::CORE_PER_TUBE; ?>
        @foreach($splitterList as $splitter)
          <?php $splitter->colorIndex = $colorIndex++ ?>
          <?php if ($colorIndex > $maxColors) $colorIndex = 1 ?>

          <li class="bg-fiber-{{ $splitter->colorIndex }}"
              data-splitter-id="{{ $splitter->id }}" data-splitter-label="{{ $splitter->label }}">
            {{ $splitter->label }}

            <div class="hidden">
              <div class="title">
                <span>Splitter</span>
                <span class="label bg-fiber-{{ $splitter->colorIndex }}">{{ $splitter->label }}</span>
              </div>
              <div class="body">
                @for ($i = 0; $i <= 4; $i++)
                  <?php $link = $splitter->ports[$i] ?? false ?>
                  @if($link)
                    <?php $token = ($i <= 0) ? $link->src_val : $link->dst_val ?>
                    <?php $token = explode(':', $token) ?>
                  @endisset

                  <?php $type = ($i <= 0) ? 'in' : 'out' ?>

                  <div class="splitter-port {{ ($link) ? 'clickable' : '' }}"
                       data-odc-panel="{{ $token[0] ?? '' }}" data-odc-port="{{ $token[1] ?? '' }}">
                    <span class="label label-success">{{ $type }}</span>
                    <span class="label label-info label-outline">{{ ($i <= 0) ? 'IN' : $i }}</span>
                    <i class="fas fa-long-arrow-alt-{{ ($i <= 0) ? 'left' : 'right' }}"></i>

                    @if ($link)
                      <span>
                      Panel
                      <span class="label label-info label-outline">{{ $token[0] }}</span>
                      Port
                      <span class="label label-info label-outline">{{ $token[1] }}</span>
                    </span>
                    @else
                      <span>KOSONG</span>
                    @endisset
                  </div>

                  @if ($i == 0)
                    <hr>
                  @endif
                @endfor
              </div>
            </div>
          </li>
        @endforeach
      </ul>
    </div>
  </div>
@endif
