@extends('app')

@section('title', 'Input User')

@section('body')
  <div class="page-header">
    <h1>
      <i class="fas fa-user-plus"></i>
      <span>Create Local User</span>
    </h1>
  </div>

  <div class="row">
    <div class="col-md-6 col-md-push-3">
      <form id="formUserCreate" class="panel form-horizontal" method="post">
        {{ csrf_field() }}

        <div class="panel-body">
          <?php $hasError = $errors->has('login') ?>
          <fieldset class="form-group form-message-light">
            <label for="input-login" class="col-sm-3 control-label">Login</label>
            <div class="col-sm-6">
              <input id="input-login" name="login" value="{{ old('login') }}" class="form-control"
                     data-msg-required="Silahkan isi nama Login" required>
              @if ($hasError)
                @foreach($errors->get('login') as $err)
                  <small class="form-message light">{{ $err }}</small>
                @endforeach
              @endif
            </div>
          </fieldset>

          <?php $hasError = $errors->has('password') ?>
          <fieldset class="form-group form-message-light">
            <label for="input-password" class="col-sm-3 control-label">Password</label>
            <div class="col-sm-6">
              <input id="input-password" name="password" class="form-control" type="password"
                     data-msg-required="Silahkan isi Password" required>
              @if ($hasError)
                @foreach($errors->get('password') as $err)
                  <small class="form-message light">{{ $err }}</small>
                @endforeach
              @endif
            </div>
          </fieldset>

          <fieldset class="form-group form-message-light">
            <label for="input-cpassword" class="col-sm-3 control-label">Confirm Password</label>
            <div class="col-sm-6">
              <input id="input-cpassword" name="cpassword" class="form-control" type="password"
                     data-rule-equalto="#input-password"
                     data-msg-equalto="Password harus sama">
            </div>
          </fieldset>

          <?php $hasError = $errors->has('nama') ?>
          <fieldset class="form-group form-message-light">
            <label for="input-nama" class="col-sm-3 control-label">Nama</label>
            <div class="col-sm-6">
              <input id="input-nama" name="nama" value="{{ old('nama') }}" class="form-control"
                     data-msg-required="Silahkan isi Nama pengguna" required>
              @if ($hasError)
                @foreach($errors->get('nama') as $err)
                  <small class="form-message light">{{ $err }}</small>
                @endforeach
              @endif
            </div>
          </fieldset>

          {{--<div class="form-group">
            <label class="col-sm-3 control-label">Company</label>
            <div class="col-sm-6">
              <select class="form-control custom-select">
                <option>Telkom Akses</option>
                <option>Mitra</option>
                <option>TELKOM</option>
              </select>
            </div>
          </div>--}}

          <?php $hasError = $errors->has('timezone') ?>
          <div class="form-group form-message-light">
            <label for="input-timezone" class="col-sm-3 control-label">Timezone</label>
            <div class="col-sm-6">
              <select name="timezone" class="custom-select form-control" required data-msg-required="Silahkan pilih zona waktu">
                @foreach($timezoneList as $key => $val)
                  <option value="{{ $key }}">{{ $val[0] }}</option>
                @endforeach
              </select>
              @if ($hasError)
                @foreach($errors->get('timezone') as $err)
                  <small class="form-message light">{{ $err }}</small>
                @endforeach
              @endif
            </div>
          </div>

          <?php $hasError = $errors->has('workzone_id') ?>
          <fieldset class="form-group form-message-light">
            <label for="input-workzone_text" class="col-sm-3 control-label">Work Zone</label>
            <div class="col-sm-6">
              <div class="input-group">
                <input id="input-workzone_id" name="workzone_id" type="hidden" value="{{ old('workzone_id') }}"
                       data-msg-required="Silahkan pilih WorkZone" required>
                <input id="input-workzone_text" name="workzone_text" class="form-control"
                       value="{{ old('workzone_text') }}" readonly>
                <span class="input-group-btn">
                  <button id="btnPick-workzone_id" class="btn btn-default" type="button">Select</button>
                </span>
              </div>
              @if ($hasError)
                @foreach($errors->get('workzone_id') as $err)
                  <small class="form-message light">{{ $err }}</small>
                @endforeach
              @endif
            </div>
          </fieldset>

          <?php $hasError = $errors->has('permission_id') ?>
          <fieldset class="form-group form-message-light">
            <label class="col-sm-3 control-label">Permission</label>
            <div class="col-sm-6">
              <select id="input-permission_id" class="form-control" name="permission_id" data-msg-required="Silahkan pilih Permission" required>
                <option></option>
                @foreach ($permissionList as $entry)
                  <option value="{{ $entry->id }}" {{ $entry->id == old('permission_id') ? 'selected' : '' }}>{{ $entry->title }}</option>
                @endforeach
              </select>
              @if ($hasError)
                @foreach($errors->get('permission_id') as $err)
                  <small class="form-message light">{{ $err }}</small>
                @endforeach
              @endif
            </div>
          </fieldset>

          <fieldset class="form-group form-message-light">
            <label for="input-permission" class="col-sm-3 control-label">Override Permission</label>
            <div class="col-sm-6">
              <textarea name="permission" class="form-control">{{ old('permission') }}</textarea>
            </div>
          </fieldset>
        </div>

        <div class="panel-footer text-right">
          <button class="btn btn-primary">
            <i class="fas fa-check"></i>
            <span>Simpan</span>
          </button>
        </div>
      </form>
    </div>
  </div>
@endsection

@section('script')
  @include('partial.workzone.modal')
  @include('partial.workzone.script', [
    'field' => 'workzone_id',
    'displayField' => 'workzone_text',
    'workzoneTree' => $workzoneTree,
    'canEdit' => true
  ])

  <script>
    $(function() {
      $('#input-permission_id').select2({
        placeholder: '...'
      });
    });

  </script>

  @include('partial.form.validate', ['id' => 'formUserCreate'])
@endsection
