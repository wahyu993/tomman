<?php

namespace App\Providers;

use App\Service\Auth\WorkzoneCached;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use App\Service\SessionHelper;
use App\Service\Auth\Authorization;
use App\Service\Auth\MenuBuilder;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrapThree();

        View::composer('*', function ($view) {
            $view->with('currentUser', SessionHelper::getCurrentUser());
        });

        View::composer('app', function ($view) {
            $view->with('mainMenu', MenuBuilder::build(SessionHelper::getCurrentUser()));
        });

        Validator::extend('valid_permission', function ($attribute, $value, $parameters, $validator) {
            return Authorization::isValidPermission($value);
        });

        Validator::extend('within_user_workzone', function ($attribute, $value, $parameters, $validator) {
            if (!$value) {
                return false;
            }

            $user = SessionHelper::getCurrentUser();
            [$targetWorkzone, $lastModified] = WorkzoneCached::getById($value);

            return Authorization::isWithinZone($user->workzone_path, $targetWorkzone->path);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
