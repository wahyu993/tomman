<?php

namespace App\Service\Mcore;

use Illuminate\Support\Facades\DB;
use App\Service\Mcore\Helper as Mcore;

class Map
{
    const CENTER_LAT = 1.2000975492067614;
    const CENTER_LNG = 118.05706805;

    const KEY_ZOOM = 'zoom';
    const KEY_SIZE = 'size';
    const KEY_LOAD = 'load';
    const KEY_VISIBLE = 'visible';
    const KEY_WIDTH = 'width';
    const KEY_HEIGHT = 'height';

    const GRID = [
        [
            // street level
            self::KEY_ZOOM => 19,
            self::KEY_SIZE => [
                self::KEY_WIDTH => 0.002816319465637207,
                self::KEY_HEIGHT => 0.0017644577717170762
            ],
            self::KEY_LOAD => [
                Mcore::TYPE_ODP,
                Mcore::TYPE_PELANGGAN,
                Mcore::TYPE_ALPRO
            ],
            self::KEY_VISIBLE => [
                Mcore::TYPE_ODC
            ]
        ],

        [
            // city level
            self::KEY_ZOOM => 13,
            self::KEY_SIZE => [
                self::KEY_WIDTH => 0.18024444580078125,
                self::KEY_HEIGHT => 0.1129268618142647
            ],
            self::KEY_LOAD => [
                Mcore::TYPE_ODC
            ]
        ],

        /*[
            // country level
            self::KEY_ZOOM => 5,
            self::KEY_SIZE => [
                self::KEY_WIDTH => 2.8839111328125,
                self::KEY_HEIGHT => 1.7879950033347125
            ],
            self::KEY_TYPES => [
                Mcore::TYPE_STO
            ]
        ]*/
    ];

    const SQL_COLUMN = [
        '*' => 'label',
        Mcore::TYPE_ALPRO => 'type',
        Mcore::TYPE_PELANGGAN => 'kode, label'
    ];

    private static function generateSqlColumn($deviceType)
    {
        $column = array_key_exists($deviceType, self::SQL_COLUMN)
                ? self::SQL_COLUMN[$deviceType]
                : self::SQL_COLUMN['*'];

        return 'SELECT id,'.$column.', ST_Y(coordinate) AS lat, ST_X(coordinate) AS lng ';
    }

    private static function getGrid($zoom)
    {
        $result = null;
        foreach (self::GRID as $grid) {
            if ($zoom >= $grid[self::KEY_ZOOM]) {
                $result = $grid;
                break;
            }
        }

        return $result;
    }

    private static function calcCellGeom($x, $y, $width, $height)
    {
        $cellCenterLat = ($y * $height) + self::CENTER_LAT;
        $cellCenterLng = ($x * $width) + self::CENTER_LNG;

        $halfHeight = $height / 2;
        $halfWidth = $width / 2;

        $n = $cellCenterLat - $halfHeight;
        $s = $cellCenterLat + $halfHeight;

        $w = $cellCenterLng - $halfWidth;
        $e = $cellCenterLng + $halfWidth;

        return [$n, $s, $w, $e];
    }

    public static function cellTest($zoom, $x, $y)
    {
        $grid = self::getGrid($zoom);
        $size = $grid[self::KEY_SIZE];
        [$n, $s, $w, $e] = self::calcCellGeom($x, $y, $size[self::KEY_WIDTH], $size[self::KEY_HEIGHT]);

        return compact('n', 's', 'w', 'e');
    }

    public static function getByGrid($deviceType, $zoom, $x, $y)
    {
        $grid = self::getGrid($zoom);
        if (!in_array($deviceType, $grid[self::KEY_LOAD])) {
            return [];
        }

        $size = $grid[self::KEY_SIZE];
        [$n, $s, $w, $e] = self::calcCellGeom($x, $y, $size[self::KEY_WIDTH], $size[self::KEY_HEIGHT]);
        $area = "ST_GeomFromText('POLYGON(($w $n, $e $n, $e $s, $w $s, $w $n))', 4326)";

        $sql  = self::generateSqlColumn($deviceType);
        $sql .= " FROM mcore.$deviceType WHERE ST_Contains($area, coordinate)";

        return DB::select($sql);
    }

    public static function getAll($deviceType)
    {
        $table = constant(Mcore::class.'::TYPE_'.strtoupper($deviceType));
        if ($table === null) {
            return [];
        }

        $sql  = self::generateSqlColumn($deviceType);
        $sql .= "FROM mcore.$table WHERE ST_X(coordinate) <> '0' AND ST_Y(coordinate) <> '0'";

        return DB::select($sql);
    }

    public static function getByIdList($deviceType, array $idList)
    {
        $table = constant(Mcore::class.'::TYPE_'.strtoupper($deviceType));
        if ($table === null) {
            return [];
        }

        $ids = [];
        foreach ($idList as $id) {
            $ids[] = intval($id);
        }
        $ids = implode(',', $ids);

        $sql  = self::generateSqlColumn($deviceType);
        $sql .= "FROM mcore.$table WHERE id IN($ids)";

        return DB::select($sql);
    }
}
