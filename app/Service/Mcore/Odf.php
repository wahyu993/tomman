<?php

namespace App\Service\Mcore;

use Illuminate\Support\Facades\DB;
use App\Service\SessionHelper;

class Odf
{
    private static function table()
    {
        return DB::table('mcore.odf');
    }

    private static function auditTable()
    {
        return DB::table('mcore.odf_audit');
    }

    public static function insertHistory($user_id, $odf_id, $operation, array $data)
    {
        self::auditTable()->insert([
            'odf_id' => $odf_id,
            'user_id' => $user_id,
            'timestamp' => DB::raw("NOW() AT TIME ZONE 'utc'"),
            'operation' => $operation,
            'data' => json_encode($data)
        ]);
    }

    /**
     * @param $userId
     * @param $roomId
     * @param $label
     * @param $group
     * @return int
     * @throws \Throwable
     */
    public static function create($userId, $roomId, $label, $group)
    {
        [$roomData, $room_mtime] = StoRoomCached::getById($roomId);
        $data = compact('label', 'group');
        $data['sto_room_id'] = $roomData->id;

        $id = 0;
        DB::transaction(function () use ($userId, $roomData, $data, &$id) {
            $id = self::table()->insertGetId($data);

            self::insertHistory($userId, $id, 'insert', $data);
            StoCached::insertHistory($userId, $roomData->sto_id, 'insert.odf', $data);
        });

        return $id;
    }

    public static function getById($id)
    {
        return self::table()->where('id', $id)->first();
    }

    /**
     * @param $userId
     * @param $roomId
     * @param $id
     * @param $label
     * @param $group
     * @throws \Throwable
     */
    public static function update($userId, $roomId, $id, $label, $group)
    {
        [$roomData, $room_mtime] = StoRoomCached::getById($roomId);

        DB::transaction(function () use ($userId, $roomData, $id, $label, $group) {
            $data = compact('id', 'label', 'group');

            StoCached::insertHistory($userId, $roomData->sto_id, 'update.odf', $data);
            self::insertHistory($userId, $id, 'update', $data);

            unset($data['id']);
            self::table()->where('id', $id)->update($data);
        });
    }

    public static function listByRoomId($id)
    {
        $rows = self::table()
            ->where('sto_room_id', $id)
            ->orderBy('group')
            ->get();

        return self::transformIntoGroup($rows);
    }

    private static function transformIntoGroup($rows)
    {
        $result = [];
        foreach ($rows as $row) {
            $result[$row->group][] = $row;
        }

        return $result;
    }
}
