<?php

namespace App\Service\Mcore\Link;

use Illuminate\Support\Facades\DB;
use App\Service\Mcore\DistribusiCached;
use App\Service\Mcore\OdcCached;
use App\Service\Mcore\OdpCached;

class OdcToOdp
{
    public const SRC_TYPE = 'odc';
    public const MED_TYPE = 'distribusi';
    public const DST_TYPE = 'odp';

    private static function table()
    {
        return DB::table('mcore.link');
    }

    private static function db()
    {
        return self::table()
            ->join('mcore.odc', function ($join) {
                $join->on('link.src_type', DB::raw("'".self::SRC_TYPE."'"))
                     ->on('link.src_id', 'odc.id');
            })
            ->join('auth.workzone AS wz_odc', 'odc.workzone_id', '=', 'wz_odc.id')

            ->join('mcore.distribusi', function ($join) {
                $join->on('link.med_type', DB::raw("'".self::MED_TYPE."'"))
                     ->on('link.med_id', 'distribusi.id');
            })
            ->join('auth.workzone AS wz_distribusi', 'distribusi.workzone_id', '=', 'wz_distribusi.id')

            ->join('mcore.odp', function ($join) {
                $join->on('link.dst_type', DB::raw("'".self::DST_TYPE."'"))
                     ->on('link.dst_id', 'odp.id');
            })
            ->join('auth.workzone AS wz_odp', 'odp.workzone_id', '=', 'wz_odp.id')

            ->select(
                'link.*',
                //
                'odc.id AS odc_id',
                'odc.label AS odc_label',
                'odc.workzone_id AS odc_workzone_id',
                'wz_odc.label AS odc_workzone_label',
                //
                'distribusi.id AS distribusi_id',
                'distribusi.label AS distribusi_label',
                'distribusi.workzone_id AS distribusi_workzone_id',
                'wz_distribusi.label AS distribusi_workzone_label',
                //
                'odp.id AS odp_id',
                'odp.label AS odp_label',
                'odp.workzone_id AS odp_workzone_id',
                'wz_odp.label AS odp_workzone_label'
            )
        ;
    }

    /**
     * @param int $userId for history/audit
     * @param int $odcId
     * @param string $odcVal
     * @param int $distribusiId
     * @param string $distribusiVal
     * @param int $odpId
     * @throws \Throwable when database transaction failed
     */
    public static function plug(
        int $userId,
        int $odcId,
        string $odcVal,
        int $distribusiId,
        string $distribusiVal,
        int $odpId
    ) {
        $data = [
            'src_type' => self::SRC_TYPE,
            'src_id' => $odcId,
            'src_val' => $odcVal,
            'med_type' => self::MED_TYPE,
            'med_id' => $distribusiId,
            'med_val' => $distribusiVal,
            'dst_type' => self::DST_TYPE,
            'dst_id' => $odpId
        ];

        DB::transaction(function () use ($userId, $data) {
            self::table()->insert($data);

            $operation = 'plug';
            OdcCached::insertHistory($userId, $data['src_id'], $operation, $data);
            DistribusiCached::insertHistory($userId, $data['med_id'], $operation, $data);
            OdpCached::insertHistory($userId, $data['dst_id'], $operation, $data);
        });
    }

    /**
     * @param int $userId for history/audit
     * @param int $odcId
     * @param string $odcVal
     * @param int $distribusiId
     * @param string $distribusiVal
     * @param int $odpId
     * @throws \Throwable when database transaction failed
     */
    public static function unplug(
        int $userId,
        int $odcId,
        string $odcVal,
        int $distribusiId,
        string $distribusiVal,
        int $odpId
    ) {
        $data = [
            'src_type' => self::SRC_TYPE,
            'src_id' => $odcId,
            'src_val' => $odcVal,
            'med_type' => self::MED_TYPE,
            'med_id' => $distribusiId,
            'med_val' => $distribusiVal,
            'dst_type' => self::DST_TYPE,
            'dst_id' => $odpId
        ];

        DB::transaction(function () use ($userId, $data) {
            self::table()->where($data)->delete();

            $operation = 'unplug';
            OdcCached::insertHistory($userId, $data['src_id'], $operation, $data);
            DistribusiCached::insertHistory($userId, $data['med_id'], $operation, $data);
            OdpCached::insertHistory($userId, $data['dst_id'], $operation, $data);
        });
    }

    public static function getByOdcPanelPort($id, $panel, $port)
    {
        return self::db()
            ->where('src_type', self::SRC_TYPE)
            ->where('src_id', $id)
            ->where('src_val', "$panel:$port:rear")
            ->first()
        ;
    }

    public static function getByDistribusi($id)
    {
        return self::db()
            ->where('med_type', self::MED_TYPE)
            ->where('med_id', $id)
            ->orderBy('med_val')
            ->get()
        ;
    }

    public static function getByOdp($id)
    {
        return self::db()
            ->where('dst_type', self::DST_TYPE)
            ->where('dst_id', $id)
            ->first()
        ;
    }
}
