<?php

namespace App\Service\Mcore;

use App\Service\Cache;
use App\Service\Auth\WorkzoneCached;

class PelangganCached
{
    public const TYPES = Pelanggan::TYPES;

    const PREFIX = 'Mcore.Pelanggan:';
    const TAG_LISTING = self::PREFIX.'List';

    const KEY_COUNT = self::PREFIX.'Count;WorkzonePath=';
    const KEY_BY_WORKZONE = self::PREFIX.'WorkzonePath=%s;Page=%s;Search=%s;Limit=%s';
    const KEY_BY_ID = self::PREFIX.'Id=';

    public static function tagById($id)
    {
        return self::KEY_BY_ID.$id;
    }

    public static function tagByWorkzoneId($workzoneId)
    {
        return WorkzoneCached::tagById($workzoneId).';'.self::TAG_LISTING;
    }

    public static function keyById($id)
    {
        return self::KEY_BY_ID.$id;
    }

    public static function countByWorkzonePath($path)
    {
        $key = self::KEY_COUNT.$path;

        $dataSource = function () use ($path) {
            return Pelanggan::countByWorkzonePath($path);
        };

        $tagGenerator = function () use ($path) {
            $id = WorkzoneCached::idByPath($path);
            $tag = self::tagByWorkzoneId($id);

            return [$tag];
        };

        return Cache::store($key, $dataSource, $tagGenerator);
    }

    /**
     * flush: WorkzoneCached::flushTagByWorkzoneId
     *
     * @param int $user_id for history/audit
     * @param int $workzone_id
     * @param string $kode
     * @param string $label
     * @param int $type
     * @param string $pic
     * @param string $alamat
     * @param string $keterangan
     * @param float $lat
     * @param float $lng
     * @return int id of newly created object
     * @throws \Throwable when database transaction failed
     */
    public static function create(
        int $user_id,
        int $workzone_id,
        string $kode,
        string $label,
        int $type,
        ?string $pic,
        ?string $alamat,
        ?string $keterangan,
        float $lat,
        float $lng
    ) {
        $id = Pelanggan::create($user_id, $workzone_id, $kode, $label, $type, $pic, $alamat, $keterangan, $lat, $lng);

        WorkzoneCached::flushTagByWorkzoneId($workzone_id, [self::class, 'tagByWorkzoneId']);

        return $id;
    }

    public static function insertHistory($user_id, $pelanggan_id, $operation, array $data)
    {
        Pelanggan::insertHistory($user_id, $pelanggan_id, $operation, $data);

        Cache::del(self::keyById($pelanggan_id));
    }

    public static function getById($id)
    {
        $key = self::keyById($id);

        $dataSource = function () use ($id) {
            return Pelanggan::getById($id);
        };

        $tagGenerator = function () use ($id) {
            return [self::tagById($id)];
        };

        return Cache::store($key, $dataSource, $tagGenerator);
    }

    /**
     * flush: WorkzoneCached::tagsByWorkzoneTree, tagById
     *
     * @param int $user_id for history/audit
     * @param int $pelanggan_id
     * @param int $workzone_id
     * @param string $kode
     * @param string $label
     * @param int $type
     * @param string $pic
     * @param string $alamat
     * @param string $keterangan
     * @param float $lat
     * @param float $lng
     * @throws \Throwable when database transaction failed
     */
    public static function update(
        int $user_id,
        int $pelanggan_id,
        int $workzone_id,
        string $kode,
        string $label,
        int $type,
        ?string $pic,
        ?string $alamat,
        ?string $keterangan,
        float $lat,
        float $lng
    ) {
        Pelanggan::update(
            $user_id,
            $pelanggan_id,
            $workzone_id,
            $kode,
            $label,
            $type,
            $pic,
            $alamat,
            $keterangan,
            $lat,
            $lng
        );

        $key = self::keyById($pelanggan_id);
        WorkzoneCached::flushIfKeyExists(
            $key,
            $workzone_id,
            'workzone_id',
            'workzone_path',
            [self::class, 'tagByWorkzoneId'],
            [self::tagById($pelanggan_id)]
        );
    }

    public static function paginateByWorkzonePath($path, $page = 1, $search = null, $limit = 25)
    {
        $ttl = isset($search) ? 60 * 60 : 0;
        $key = sprintf(self::KEY_BY_WORKZONE, $path, $page, $search, $limit);

        $dataSource = function () use ($path, $page, $search, $limit) {
            return Pelanggan::paginateByWorkzonePath($path, $page, $search, $limit);
        };

        $tagGenerator = function () use ($path) {
            return WorkzoneCached::tagsByWorkzonePath($path, [self::class, 'tagByWorkzoneId']);
        };

        return Cache::store($key, $dataSource, $tagGenerator, $ttl);
    }
}
