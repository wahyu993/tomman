<?php

namespace App\Service\Mcore;

use App\Service\Cache;
use App\Service\Auth\WorkzoneCached;

class OdpCached
{
    public const STATUSES = Odp::STATUSES;
    public const TYPES = Odp::TYPES;
    public const CAPACITIES = Odp::CAPACITIES;

    const PREFIX = 'Mcore.Odp:';
    const TAG_LISTING = self::PREFIX.'List';

    const KEY_COUNT = self::PREFIX.'Count;WorkzonePath=';
    const KEY_BY_ID = self::PREFIX.'Id=';
    const KEY_BY_WORKZONE = self::PREFIX.'WorkzonePath=%s;Page=%s;Search=%s;Limit=%s';
    const KEY_BY_WORKZONE_WITH_SOURCE = self::KEY_BY_WORKZONE.';+SourceLink';

    public static function tagById($id)
    {
        return self::KEY_BY_ID.$id;
    }

    public static function tagByWorkzoneId($workzoneId)
    {
        return WorkzoneCached::tagById($workzoneId).';'.self::TAG_LISTING;
    }

    public static function keyById($id)
    {
        return self::KEY_BY_ID.$id;
    }

    public static function countByWorkzonePath($path)
    {
        $key = self::KEY_COUNT.$path;

        $dataStore = function () use ($path) {
            return Odp::countByWorkzonePath($path);
        };

        $tagGenerator = function () use ($path) {
            $id = WorkzoneCached::idByPath($path);
            $tag = self::tagByWorkzoneId($id);

            return [$tag];
        };

        return Cache::store($key, $dataStore, $tagGenerator);
    }

    /**
     * flush: WorkzoneCached::flushTagByWorkzoneId
     *
     * @param int $user_id for history/audit
     * @param int $workzone_id
     * @param string $label
     * @param string $tenoss
     * @param int $type
     * @param int $status
     * @param int $capacity
     * @param float $lat
     * @param float $lng
     * @return int
     * @throws \Throwable when database transaction failed
     */
    public static function create(
        int $user_id,
        int $workzone_id,
        string $label,
        string $tenoss,
        int $type,
        int $status,
        int $capacity,
        float $lat,
        float $lng
    ) {
        $id = Odp::create($user_id, $workzone_id, $label, $tenoss, $type, $status, $capacity, $lat, $lng);

        WorkzoneCached::flushTagByWorkzoneId($workzone_id, [self::class, 'tagByWorkzoneId']);

        return $id;
    }

    public static function insertHistory($user_id, $odp_id, $operation, array $data)
    {
        Odp::insertHistory($user_id, $odp_id, $operation, $data);

        Cache::del(self::keyById($odp_id));
    }

    public static function getById($id)
    {
        $key = self::keyById($id);

        $dataSource = function () use ($id) {
            return Odp::getById($id);
        };

        $tagGenerator = function () use ($id) {
            return [self::tagById($id)];
        };

        return Cache::store($key, $dataSource, $tagGenerator);
    }

    /**
     * flush: WorkzoneCached::tagsByWorkzoneTree, tagById
     *
     * @param int $user_id for history/audit
     * @param int $odp_id
     * @param int $workzone_id
     * @param string $label
     * @param string $tenoss
     * @param int $type
     * @param int $status
     * @param int $capacity
     * @param float $lat
     * @param float $lng
     * @throws \Throwable when database transaction failed
     */
    public static function update(
        int $user_id,
        int $odp_id,
        int $workzone_id,
        string $label,
        ?string $tenoss,
        int $type,
        int $status,
        int $capacity,
        float $lat,
        float $lng
    ) {
        Odp::update($user_id, $odp_id, $workzone_id, $label, $tenoss, $type, $status, $capacity, $lat, $lng);

        $key = self::keyById($odp_id);
        WorkzoneCached::flushIfKeyExists(
            $key,
            $workzone_id,
            'workzone_id',
            'workzone_path',
            [self::class, 'tagByWorkzoneId'],
            [self::tagById($odp_id)]
        );
    }

    public static function paginateByWorkzonePath($path, $page = 1, $search = null, $limit = 25)
    {
        $ttl = isset($search) ? 60 * 60 : 0;
        $key = sprintf(self::KEY_BY_WORKZONE, $path, $page, $search, $limit);

        $dataSource = function () use ($path, $page, $search, $limit) {
            return Odp::paginateByWorkzonePath($path, $page, $search, $limit);
        };

        $tagGenerator = function () use ($path) {
            return WorkzoneCached::tagsByWorkzonePath($path, [self::class, 'tagByWorkzoneId']);
        };

        return Cache::store($key, $dataSource, $tagGenerator, $ttl);
    }

    public static function paginateByWorkzonePathWithSourceLink($path, $page = 1, $search = null, $limit = 25)
    {
        $ttl = isset($search) ? 60 * 60 : 0;
        $key = sprintf(self::KEY_BY_WORKZONE_WITH_SOURCE, $path, $page, $search, $limit);

        $dataSource = function () use ($path, $page, $search, $limit) {
            return Odp::paginateByWorkzonePathWithSourceLink($path, $page, $search, $limit);
        };

        $tagGenerator = function () use ($path) {
            return WorkzoneCached::tagsByWorkzonePath($path, [self::class, 'tagByWorkzoneId']);
        };

        return Cache::store($key, $dataSource, $tagGenerator, $ttl);
    }

    /**
     * Not Cached
     *
     * @param $capacity
     * @param $links
     * @return array
     */
    public static function linksToPorts($capacity, $links)
    {
        return Odp::linksToPorts($capacity, $links);
    }
}
