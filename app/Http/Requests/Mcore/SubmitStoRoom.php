<?php

namespace App\Http\Requests\Mcore;

use Illuminate\Foundation\Http\FormRequest;
use App\Service\SessionHelper;
use App\Service\Auth\Authorization;

class SubmitStoRoom extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return SessionHelper::currentUserHasPermission('mcore.sto', Authorization::WRITE);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'label' => 'required'
        ];
    }
}
