<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Service\CoordinateHelper;

class FormRequestWithCoordinate extends FormRequest
{
    protected function prepareForValidation()
    {
        $request = $this->request;
        $coordinate = $this->input('coordinate');
        $latLng = CoordinateHelper::parseLatLngFromText($coordinate);

        $request->remove('coordinate');
        $request->add($latLng);
    }
}
