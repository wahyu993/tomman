<?php

namespace App\Http\Middleware;

use Closure;
use App\Service\SessionHelper;
use App\Service\Auth\Authorization;

class HasPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  \String  $module
     * @param  \String  $permission
     * @return mixed
     */
    public function handle($request, Closure $next, $module, $permission)
    {
        $currentUser = SessionHelper::getCurrentUser();
        $perm = Authorization::parsePermission($permission);

        if (Authorization::hasPermission($module, $perm, $currentUser->permission)) {
            return $next($request);
        } else {
            return abort(403);
        }
    }
}
