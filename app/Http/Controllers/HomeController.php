<?php

namespace App\Http\Controllers;

use App\Service\SessionHelper;
use App\Service\Auth\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $user = SessionHelper::getCurrentUser();

        if ($user->type == User::TYPE_NEWUSER) {
            return $this->unassignedView();
        } elseif (!$user->is_login_enabled) {
            return $this->lockedView();
        }

        return view('home.default');
    }

    private function unassignedView()
    {
        return view('home.newuser');
    }

    private function lockedView()
    {
        return view('home.locked');
    }
}
