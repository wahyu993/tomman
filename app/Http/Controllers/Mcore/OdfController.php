<?php

namespace App\Http\Controllers\Mcore;

use App\Http\Controllers\Controller;
use App\Http\Requests\Mcore\SubmitOdf;
use App\Service\SessionHelper;
use App\Service\Auth\Authorization;
use App\Service\Mcore\StoCached;
use App\Service\Mcore\StoRoomCached;
use App\Service\Mcore\OdfCached;
use App\Service\Mcore\OdfPanelCached;

class OdfController extends Controller
{
    public function createForm($stoId, $roomId)
    {
        [$stoData, $sto_mtime] = StoCached::getById($stoId);
        [$roomData, $room_mtime] = StoRoomCached::getById($roomId);
        $canEdit = true;

        return view('mcore.odf.form', compact('stoData', 'roomData', 'canEdit'));
    }

    public function create(SubmitOdf $request, $stoId, $roomId)
    {
        try {
            $user = SessionHelper::getCurrentUser();
            $id = OdfCached::create($user->id, $roomId, $request->label, $request->group);

            $url = "/mcore/sto/$stoId/room/$roomId/odf/$id";
            return back()->with('alerts', [
                [
                    'type' => 'success',
                    'text' => "<strong>Berhasil</strong> menambah data <a href='$url'>{$request->label}</a>"
                ]
            ]);
        } catch (\Throwable $e) {
            return back()->withInput()->with('alerts', [
                [
                    'type' => 'danger',
                    'text' => '<strong>GAGAL</strong> menambah data<br>'.$e->getMessage()
                ]
            ]);
        }
    }

    public function updateForm($stoId, $roomId, $odfId)
    {
        [$stoData, $sto_mtime] = StoCached::getById($stoId);
        [$roomData, $room_mtime] = StoRoomCached::getById($roomId);
        [$odfData, $odf_mtime] = OdfCached::getById($odfId);
        [$panelList, $otb_mtime] = OdfPanelCached::listByOdf($odfId);

        $currentUser = SessionHelper::getCurrentUser();
        $hasWriteAccess = SessionHelper::currentUserHasPermission('mcore.sto.odf', Authorization::WRITE);
        $isWithinZone = Authorization::isWithinZone($currentUser->workzone_path, $stoData->workzone_path);
        $canEdit = $hasWriteAccess && $isWithinZone;

        return view('mcore.odf.form', compact('stoData', 'roomData', 'odfData', 'panelList', 'canEdit'));
    }

    public function update(SubmitOdf $request, $stoId, $roomId, $odfId)
    {
        try {
            $user = SessionHelper::getCurrentUser();
            OdfCached::update($user->id, $roomId, $odfId, $request->label, $request->group);

            return back()->with('alerts', [
                [
                    'type' => 'success',
                    'text' => '<strong>Berhasil</strong> merubah data'
                ]
            ]);
        } catch (\Throwable $e) {
            return back()->withInput()->with('alerts', [
                [
                    'type' => 'danger',
                    'text' => '<strong>GAGAL</strong> merubah data<br>'.$e->getMessage()
                ]
            ]);
        }
    }

    public function listPanelByOdfAsSelect2($odfId)
    {
        [$panelList, $otb_mtime] = OdfPanelCached::listByOdf($odfId);

        $result = [];
        foreach ($panelList->all() as $otb) {
            $otb->text = $otb->label;

            $result[] = $otb;
        }

        return $result;
    }
}
