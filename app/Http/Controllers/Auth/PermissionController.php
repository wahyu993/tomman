<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreatePermission;
use App\Http\Requests\UpdatePermission;
use App\Service\Auth\Permission;
use App\Service\Auth\Authorization;
use App\Service\SessionHelper;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    public function index(Request $request)
    {
        $currentUser = SessionHelper::getCurrentUser();
        $isWritable = Authorization::hasPermission(
            'auth.permission',
            Authorization::WRITE,
            $currentUser->permission
        );

        $q = $request->input('q');
        if ($q) {
            $list = Permission::paginateWithSearch($q);
            $list->appends(['q' => $q]);
        } else {
            $list = Permission::paginate();
        }

        return view('auth.permission', compact('list', 'q', 'isWritable'));
    }

    public function create(CreatePermission $request)
    {
        $title = $request->input('title');
        $permission = $request->input('permission');

        Permission::create($title, $permission);

        return back()->with('alerts', [
            [
                'type' => 'success',
                'text' => '<strong>Berhasil</strong> menambah data'
            ]
        ]);
    }

    public function update(UpdatePermission $request)
    {
        $id = $request->input('id');
        $title = $request->input('title');
        $permission = $request->input('permission');

        Permission::update($id, $title, $permission);

        return back()->with('alerts', [
            [
                'type' => 'success',
                'text' => '<strong>Berhasil</strong> merubah data'
            ]
        ]);
    }

    public static function parseForDisplay($permissionText)
    {
        $result = [];

        $lines = explode("\n", $permissionText);
        foreach ($lines as $line) {
            $line = trim($line);
            if (!$line) {
                continue;
            }

            $token = explode(':', $line);
            $perms = explode(',', $token[1]);

            $perms = array_map(__CLASS__.'::parseIndividualPermissionForDisplay', $perms);

            $result[] = (object)[
                'module' => strtoupper(trim($token[0])),
                'permission' => $perms
            ];
        }

        return $result;
    }

    public static function parseIndividualPermissionForDisplay($item)
    {
        $label = strtoupper(trim($item));
        $class = 'info';

        switch ($label) {
            case 'READ':
            case 'READ_DATA':
                $class = 'success';
                break;

            case 'WRITE':
            case 'WRITE_DATA':
                $class = 'warning';
                break;

            case 'WRITE_AUDIT':
            case 'AUDIT':
            case 'FULL':
            case 'ALL':
                $class = 'primary';
                break;
        }

        return (object)compact('label', 'class');
    }
}
